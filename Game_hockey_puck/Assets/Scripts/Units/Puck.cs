﻿using UnityEngine;

namespace Units
{
    public class Puck : MonoBehaviour
    {
        [Range(1, 250)] [SerializeField] private float force;
        [Range(0, 100)] [SerializeField] private float bouncedPercent;
        private Rigidbody2D _rigidbody2D;

        void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }
    
        public void Move(Vector2 dictionary)
        {
            dictionary = new Vector2(dictionary.x * -1, dictionary.y);

            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.AddForce(dictionary * force * 10, ForceMode2D.Impulse);
        }

        void OnCollisionEnter2D(Collision2D hitObj)
        {
            var bounced = bouncedPercent / 100;
            _rigidbody2D.velocity = Vector2.Reflect(-hitObj.relativeVelocity, hitObj.contacts[0].normal) * bounced;
        }
    }
}