﻿using UnityEngine;

namespace Units
{
    public class Scaling : MonoBehaviour
    {
        [SerializeField] private Vector3 startScale;
        [SerializeField] private Vector3 targetScale;
        [SerializeField] private float duration;

        private State _currentState;

        private enum State
        {
            Scale,
            None
        }

        private void Start()
        {
            _currentState = State.Scale;
            transform.localScale = startScale;
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (_currentState == State.Scale)
            {
                Scale();
            }
            else if (_currentState == State.None)
            {
                Destroy(this);
            }
        }

        private void Scale()
        {
            var steps = (targetScale - startScale) /
                        (duration / Time.fixedDeltaTime);
            if (Vector3.Distance(transform.localScale, targetScale) >= 0.1f)
            {
                transform.localScale += steps;
            }
            else
            {
                transform.localScale = targetScale;
                _currentState = State.None;
            }
        }
    }
}