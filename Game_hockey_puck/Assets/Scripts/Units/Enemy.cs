﻿using System;
using Other;
using UnityEngine;

namespace Units
{
    public class Enemy : MonoBehaviour
    {
        private Action _action;
    

        public void SetCollisionListener(Action action)
        {
            _action = action;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == Tag.Puck.ToString())
            {
                _action?.Invoke();
            }
        }
    }
}