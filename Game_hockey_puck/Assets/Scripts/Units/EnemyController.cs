﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Units
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private int maxEnemies = 20;

        [SerializeField] private UnityEvent onLoss;
    
        private BoxCollider2D _boxCollider;
        private Vector2 _sizeEnemyController;
        private List<Enemy> _enemies = new List<Enemy>();



        private void Awake()
        {
            _boxCollider = GetComponent<BoxCollider2D>();
            _sizeEnemyController = _boxCollider.size;
        }

        public void SpawnEnemies(Enemy enemyPrefab, int indexLevel)
        {
            foreach (var enemy in _enemies)
            {
                Destroy(enemy.gameObject);
            }
            _enemies.Clear();

            var numEnemies = GetNumEnemiesByIndexLevel(indexLevel);
            for (int i = 0; i < numEnemies; i++)
            {
                var enemy = Instantiate(enemyPrefab, transform);
                enemy.transform.localPosition = new Vector2(
                    Random.Range(-_sizeEnemyController.x / 2, _sizeEnemyController.x / 2),
                    Random.Range(-_sizeEnemyController.y / 2, _sizeEnemyController.y / 2));
                enemy.SetCollisionListener(() =>
                {
                    onLoss?.Invoke();
                });
                _enemies.Add(enemy);
            }
        }
    
        private int GetNumEnemiesByIndexLevel(int indexLevel)
        {
            //1-5 - 2 врага
            //4-6 - 3 врага
            //7-9 - 4 врага
            var numEnemies = (indexLevel - 1) / 5 + 2;
            return Mathf.Clamp(numEnemies, 2, maxEnemies);
        }
    }
}