﻿using Level;
using Other;
using UI;
using Units;
using UnityEngine;

public class MainController : MonoBehaviour
{
    public static int CurrentLevel
    {
        get => PlayerPrefs.GetInt("CurrentLevel", 1);
        private set => PlayerPrefs.SetInt("CurrentLevel", value);
    }

    [SerializeField] private Puck puck;
    [SerializeField] private Enemy enemy;

    private LevelController _levelController;

    private void Start()
    {
        _levelController = FindObjectOfType<LevelController>();
        _levelController.Init(puck, enemy);
        LoadLevel();
    }

    public void OnGoal()
    {
        Statistic.LevelComplete(CurrentLevel);
        CurrentLevel++;
        LoadLevel();
    }

    public void LoadLevel()
    {
        _levelController.UpdateLevel(CurrentLevel);
        ControllerUi.Instance?.UpdateLevel(CurrentLevel);
    }
}
