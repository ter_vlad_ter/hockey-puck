﻿using System;
using Other;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace Level
{
    public class GoalArea : MonoBehaviour
    {
        [SerializeField] private UnityEvent onGoal;

        [Header("Пределы для размера ворот")] [SerializeField]
        private float maxSize;

        [SerializeField] private float minSize;

        [Header("Пределы положения ворот")] [SerializeField]
        private float maxY;

        [SerializeField] private float minY;

        private State _currentState = State.None;
        private Vector3 _targetLocalPosition;

        private enum State
        {
            Move,
            None
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == Tag.Puck.ToString())
            {
                onGoal?.Invoke();
            }
        }

        public void UpdateGoalArea(int indexLevel)
        {
            UpdateSizeByIndexLevel(indexLevel);
            UpdatePosition();
        }

        private void UpdateSizeByIndexLevel(int indexLevel)
        {
            var step = (maxSize - minSize) / 50;
            var currentSize = maxSize - (indexLevel - 1) * step;
            currentSize = Mathf.Clamp(currentSize, minSize, maxSize);
            transform.localScale = new Vector3(1, currentSize, 1);
        }

        private void UpdatePosition()
        {
            var range = (-minY + maxY);
            range -= transform.localScale.y;
            var minPositionY = -range / 2;
            var maxPositionY = range / 2;

            var positionY = Random.Range(minPositionY, maxPositionY);
            _targetLocalPosition = new Vector3(0, positionY, 0);
            _currentState = State.Move;
        }

        private void Update()
        {
            if (_currentState == State.Move)
            {
                Move();
            }
        }

        private void Move()
        {
            if (Vector3.Distance(transform.localPosition, _targetLocalPosition) >= 0.2)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, _targetLocalPosition, 0.05f);
            }
            else
            {
                _currentState = State.None;
            }
        }
    }
}