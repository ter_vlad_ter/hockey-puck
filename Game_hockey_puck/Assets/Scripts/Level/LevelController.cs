﻿using Units;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Level
{
    public class LevelController : MonoBehaviour
    {
        private PointSpawn _pointSpawn;
        private Puck _puck;
        private GoalArea _goalArea;
        private EnemyController _enemyController;

        private Puck _prefabPuck;
        private Enemy _prefabEnemy;


        public void Init(Puck puck, Enemy enemy)
        {
            _prefabPuck = puck;
            _prefabEnemy = enemy;
        }

        public void UpdateLevel(int indexLevel)
        {
            _goalArea?.UpdateGoalArea(indexLevel);
            SpawnUnits(indexLevel);
        }


        public void OnSwipe(Vector2 dir)
        {
            var screenSize = new Vector2(Screen.width, Screen.height);

            _puck.Move(dir / screenSize);
        }
    
        private void Awake()
        {
            _pointSpawn = FindObjectOfType<PointSpawn>();
            _goalArea = FindObjectOfType<GoalArea>();
            _enemyController = FindObjectOfType<EnemyController>();
        }

        private void SpawnUnits(int indexLevel)
        {
            if (_puck)
                Destroy(_puck.gameObject);
            if (_prefabPuck)
                _puck = Instantiate(_prefabPuck, _pointSpawn.transform);

            _enemyController.SpawnEnemies(_prefabEnemy, indexLevel);
        }
    
    
    }
}