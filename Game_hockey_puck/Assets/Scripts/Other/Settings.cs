﻿using GameAnalyticsSDK;
using UnityEngine;

namespace Other
{
    public class Settings : MonoBehaviour
    {

        [SerializeField] private ScreenOrientation screenOrientation = ScreenOrientation.Portrait;
        void Awake()
        {
            GameAnalytics.Initialize();
            Physics2D.gravity = Vector3.zero;
            Screen.orientation = screenOrientation;
            DontDestroyOnLoad(this);
        }

    
    }
}