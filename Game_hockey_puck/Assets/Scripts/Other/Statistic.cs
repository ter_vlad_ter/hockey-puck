﻿using GameAnalyticsSDK;
using UnityEngine;

namespace Other
{
    public static class Statistic
    {
        public static void LevelComplete(int indexLevel)
        {
            GameAnalytics.NewDesignEvent("Level Passed", indexLevel);
            Debug.Log(indexLevel);
        }
    }
}
