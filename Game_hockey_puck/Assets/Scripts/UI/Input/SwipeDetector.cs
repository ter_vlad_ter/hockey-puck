﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI.Input
{
    public class SwipeDetector : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {

        [SerializeField] private UnityEvent <Vector2> onSwipeDetected;


        private Vector2 _pointStartSwipe = Vector3.zero;
        private Vector2 _pointEndSwipe = Vector3.zero;
        private Vector2 _directionSwipe = Vector2.zero;
    
    
        public void OnBeginDrag(PointerEventData eventData)
        {
            _pointStartSwipe = eventData.position;

        }

        public void OnDrag(PointerEventData eventData)
        {
        
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _pointEndSwipe = eventData.position;
            _directionSwipe = _pointEndSwipe - _pointStartSwipe;
        
            onSwipeDetected?.Invoke(_directionSwipe);
        
        }
    }
}
