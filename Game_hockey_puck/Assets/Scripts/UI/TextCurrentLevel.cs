﻿using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TextMeshPro))]
    public class TextCurrentLevel : MonoBehaviour
    {
        private TextMeshPro _text;
        private void Awake()
        {
            _text = GetComponent<TextMeshPro>();
        }
    
        public string Text
        {
            get => _text.text;
            set => _text.text = value;
        }
    }
}
