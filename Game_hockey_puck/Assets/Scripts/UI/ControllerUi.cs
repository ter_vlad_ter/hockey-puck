﻿using UnityEngine;

namespace UI
{
    public class ControllerUi : MonoBehaviour
    {
        public static ControllerUi Instance { get; private set; }
        private TextCurrentLevel _textCurrentLevel;

        private void Awake()
        {
            Instance = this;
            _textCurrentLevel = FindObjectOfType<TextCurrentLevel>();
        }


        public void UpdateLevel(int currentLevel)
        {
            if (_textCurrentLevel)
                _textCurrentLevel.Text = currentLevel.ToString();
        }
    }
}